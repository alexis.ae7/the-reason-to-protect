﻿using UnityEngine;
using System.Collections;

public class FlyCameraControl : MonoBehaviour {
	public float Speed=1F;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		float h = Input.GetAxis ("Horizontal");
		float v = Input.GetAxis ("Vertical");
		float AddSpeed = 1F;
		if(Input.GetKey (KeyCode.LeftShift))AddSpeed=3F;
		transform.Translate ((Vector3.forward*v+Vector3.right*h).normalized*Speed*AddSpeed);
	}
}
