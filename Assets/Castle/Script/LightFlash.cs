﻿using UnityEngine;
using System.Collections;

public class LightFlash : MonoBehaviour {
	public float FlashSpeed=20F;
	public float FlashRange=0.3F;
	Light MyLight;
	float OriInd;
	float Target=0F;
	// Use this for initialization
	void Start () {
		MyLight = GetComponent<Light> ();
		OriInd = MyLight.intensity;
		Target = OriInd;
	}
	
	// Update is called once per frame
	void Update () {
		if (Target == MyLight.intensity) {
			Target = OriInd + Random.Range (-OriInd * FlashRange, OriInd * FlashRange);
		} else {
			MyLight.intensity=Mathf.MoveTowards(MyLight.intensity,Target,Time.deltaTime*FlashSpeed);
		}
	}
}
