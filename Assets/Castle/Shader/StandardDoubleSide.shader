﻿Shader "Custom/StandardDoubleSide" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_BumpMap ("BumpMap", 2D) = "bump" {}
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_MaskMap ("MaskMap", 2D) = "white" {}
		_DetailMap ("DetailMap", 2D) = "bump" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		cull back
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex,_BumpMap,_MaskMap,_DetailMap;

		struct Input {
			float2 uv_MainTex;
			float2 uv_DetailMap;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			fixed4 m = tex2D (_MaskMap, IN.uv_MainTex);
			fixed4 b = tex2D (_BumpMap, IN.uv_MainTex);
			fixed4 d = tex2D (_DetailMap, IN.uv_DetailMap);
			o.Albedo = c.rgb;
			o.Occlusion= m.rgb;
			o.Normal = UnpackNormal (b)+UnpackNormal (d)*m.a;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
		}
		ENDCG
		
		cull front
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex,_BumpMap,_MaskMap,_DetailMap;

		struct Input {
			float2 uv_MainTex;
			float2 uv_DetailMap;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color*0.2;
			fixed4 m = tex2D (_MaskMap, IN.uv_MainTex);
			fixed4 b = tex2D (_BumpMap, IN.uv_MainTex);
			fixed4 d = tex2D (_DetailMap, IN.uv_DetailMap);
			o.Albedo = c.rgb;
			o.Occlusion= m.rgb*0.2;
			o.Normal = UnpackNormal (b)+UnpackNormal (d)*m.a;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
		}
		ENDCG
	} 
	
	FallBack "Diffuse"
}
