Please check the demo scene in "Castle/scene/Demo.unity", You can either directly use the castle model set in that scene, or you can use the prefabsin "Castle/Prefab" folder to build your own castle!

NOTE: This pack doesn't include the water, skybox, post effects used in the screenshots and demo.

You can get those excellent packs from here:
== SUIMONO Water System :
https://www.assetstore.unity3d.com/cn/#!/content/4387
== Time of Day
https://www.assetstore.unity3d.com/cn/#!/content/7316
== Scion - Filmic Post Processing
https://www.assetstore.unity3d.com/cn/#!/content/41369