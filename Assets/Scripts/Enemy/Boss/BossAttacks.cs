using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SG
{
    public class BossAttacks : CharacterManager
    {
        [Header("Control")]
        public bool AI;
        public AudioSource enemySound;

        [Header("References")]
        public Transform model;
        public Transform player;
        public BoxCollider leftFootCollider;
        public Transform spellPosition;
        public Transform impactPosition;
        public Animator playerAnim;
        public DamageDealer greatSword;

        [Header("Attacks")]
        public GameObject earthShatterPrefab;
        public GameObject magicSwordFromSky;
        public GameObject spell;
        public GameObject auraMagic;
        public GameObject screamMagic;
        public GameObject magicFarSword;
        public GameObject impactPrefab;

        private Animator anim;

        [Header("AI Manager")]
        public float nearValue;
        public float farValue;
        public float chillTime;
        private string action;
        private float lastActionTime;
        private float distance;
        private float chillDirection;
        private bool phase2;
        private bool canBeginAI;
        private int lastAttack;

        // SlowBossDown
        private bool slowDown;
        private string actionAfterSlowDown;


        private void Start()
        {
            anim = model.GetComponent<Animator>();

            enemySound = GameObject.Find("BattleMusic").GetComponent<AudioSource>();

            Vector3 size = new Vector3(0f, 2.09f, 0.013f);
            Vector3 center = new Vector3(0f, 1.2f, 0f);
        }

        private void Update()
        {
            if (player != null)
            {
                distance = Vector3.Distance(model.transform.position, player.transform.position);

                //this.transform.position = new Vector3(transform.position.x, 0, transform.position.z);

                if (distance < 50 && !anim.GetBool("Equipped"))
                {
                    anim.SetTrigger("DrawSword");
                    enemySound.enabled = true;
                    StartCoroutine(StartAI());
                }

                if (!anim.GetBool("Equipped")) return;

                if (!canBeginAI) return;

                if (AI && !playerAnim.GetBool("isDead"))
                {
                    AI_Manager();
                }
                else
                {
                    anim.SetBool("GameEnd", true);
                    anim.SetBool("CanRotate", false);
                }

                greatSword.damageOn = anim.GetBool("Attacking");

                phase2 = anim.GetBool("Phase2");
            }
        }

        IEnumerator StartAI()
        {
            yield return new WaitForSeconds(4);
            canBeginAI = true;
        }

        private void FarAttack()
        {
            anim.SetFloat("Vertical", 0);
            anim.SetFloat("Horizontal", 0);

            int rand = 0;
            do
            {
                if (!anim.GetBool("Phase2")) rand = Random.Range(0, 7);
                if (anim.GetBool("Phase2")) rand = Random.Range(0, 8);
            } while (rand == lastAttack);
            lastAttack = rand;

            if (anim.GetBool("Phase2") && Random.Range(0, 2) == 0)
            {
                anim.SetTrigger("Spell");
            }

            switch (rand)
            {
                case 0:
                    anim.SetTrigger("CastMagicSwords");
                    break;
                case 1:
                    anim.SetTrigger("Casting");
                    break;
                case 2:
                    anim.SetTrigger("Dash");
                    break;
                case 3:
                    anim.SetTrigger("DoubleDash");
                    break;
                case 4:
                    anim.SetTrigger("Spell");
                    break;
                case 5:
                    anim.SetTrigger("Scream");
                    break;
                case 6:
                    anim.SetTrigger("Fishing");
                    break;
                case 7:
                    anim.SetTrigger("SuperSpinner");
                    break;
                default:
                    break;
            }

            action = "Wait";
        }

        private void NearAttack()
        {
            anim.SetFloat("Vertical", 0);
            anim.SetFloat("Horizontal", 0);

            int rand = 0;
            do
            {
                if (!anim.GetBool("Phase2")) rand = Random.Range(0, 10);
                if (anim.GetBool("Phase2")) rand = Random.Range(0, 13);
            } while (rand == lastAttack);
            lastAttack = rand;

            switch (rand)
            {
                case 0:
                    anim.SetTrigger("DoubleDash");
                    break;
                case 1:
                    anim.SetTrigger("Dash");
                    break;
                case 2:
                    anim.SetTrigger("SpinAttack");
                    break;
                case 3:
                    anim.SetTrigger("Combo");
                    break;
                case 4:
                    anim.SetTrigger("Casting");
                    break;
                case 5:
                    anim.SetTrigger("Combo1");
                    break;
                case 6:
                    anim.SetTrigger("Spell");
                    break;
                case 7:
                    anim.SetTrigger("AuraCast");
                    break;
                case 8:
                    anim.SetTrigger("ForwardAttack");
                    break;
                case 9:
                    anim.SetTrigger("Scream");
                    break;
                case 10:
                    anim.SetTrigger("Impact");
                    break;
                case 11:
                    anim.SetTrigger("Strong");
                    break;
                case 12:
                    anim.SetTrigger("JumpAttack");
                    break;
                default:
                    break;
            }

            action = "Wait";

        }

        private void SlowBossDown()
        {
            if (anim.GetFloat("Vertical") <= 0.4f)
            {
                slowDown = false;
                if (actionAfterSlowDown == "CallNextMove")
                {
                    action = "Wait";
                    anim.SetFloat("Vertical", 0);
                    anim.SetFloat("Horizontal", 0);
                    StartCoroutine(WaitAfterNearMove());
                }
                else if (actionAfterSlowDown == "FarAttack")
                {
                    action = "FarAttack";
                }
                else
                {
                    Debug.LogError("Not supposed to be here");
                }
            }
            else
            {
                anim.SetFloat("Vertical", Mathf.Lerp(anim.GetFloat("Vertical"), 0, 1 * Time.deltaTime));
            }
        }

        IEnumerator WaitAfterNearMove()
        {
            slowDown = false;
            action = "Wait";
            anim.SetFloat("Vertical", 0);
            anim.SetFloat("Horizontal", 0);
            float maxWaitTime = 6;
            float possibility = 2;
            if (anim.GetBool("Phase2"))
            {
                maxWaitTime = 5.5f;
                possibility = 2;
            }
            float waitTime;
            float decision = Random.Range(0, possibility);
            if (decision == 0) waitTime = Random.Range(2.5f, maxWaitTime);
            else waitTime = 0;
            yield return new WaitForSeconds(waitTime);
            action = "NearAttack";
            CallNextMove();
        }

        private void MoveToPlayer()
        {

            anim.SetFloat("Horizontal", 0);

            float speedValue = distance / 15;
            if (speedValue > 1) speedValue = 1;


            if (slowDown)
            {
                SlowBossDown();
                return;
            }

            if (distance < nearValue)
            {
                //anim.SetFloat("Vertical", 0);
                //CallNextMove();

                actionAfterSlowDown = "CallNextMove";
                slowDown = true;
            }
            else if (Time.time - lastActionTime > chillTime)
            {
                //anim.SetFloat("Vertical", 0);
                //action = "FarAttack";

                actionAfterSlowDown = "FarAttack";
                slowDown = true;
            }
            else
            {
                anim.SetFloat("Vertical", speedValue);
            }
        }

        private void WaitForPlayer()
        {

            anim.SetFloat("Horizontal", chillDirection);
            anim.SetFloat("Vertical", 0);

            if ((distance <= nearValue && Time.time - lastActionTime > chillTime) && !phase2)
            {
                CallNextMove();
            }
            else

            if ((distance > farValue && Time.time - lastActionTime > chillTime) && !phase2)
            {
                FarAttack();
            }
            else

            if ((Time.time - lastActionTime > chillTime) || phase2 && Time.time - lastActionTime > chillTime)
            {
                int rand = Random.Range(0, 3);

                if (rand % 2 == 0)
                {
                    NearAttack();
                }
                else if (rand % 2 == 1)
                {
                    FarAttack();
                }
            }

        }

        private void AI_Manager()
        {
            if (action == "Wait" || anim.GetBool("Dead") || anim.GetBool("Transposing")) return;

            if (action == "Move")
            {
                MoveToPlayer();
            }

            if (action == "WaitForPlayer")
            {
                WaitForPlayer();
            }

            if (action == "FarAttack")
            {
                if (!anim.GetBool("TakingDamage"))
                    FarAttack();
            }

            if (action == "NearAttack")
            {
                if (!anim.GetBool("TakingDamage"))
                {
                    NearAttack();
                }
            }
        }

        public void CallNextMove()
        {
            lastActionTime = Time.time;

            if (distance >= farValue && !anim.GetBool("Dead"))
            {
                action = "Move";
            }
            else if (distance > nearValue && distance < farValue && !anim.GetBool("Dead"))
            {
                int rand = Random.Range(0, 2);
                if (rand == 0) chillDirection = -0.5f;
                if (rand == 1) chillDirection = 0.5f;
                action = "WaitForPlayer";
            }
            else if (distance <= nearValue && !anim.GetBool("Dead"))
            {
                action = "NearAttack";
            }
        }

        private bool IsBossTakingDamage()
        {
            return !anim.GetCurrentAnimatorStateInfo(2).IsName("none");
        }

        #region Magics
        public void SpawnEarthShatter()
        {
            Vector3 bossPos = model.transform.position;
            Vector3 bossDirection = model.transform.forward;
            Quaternion bossRotation = model.transform.rotation;
            float spawnDistance = 3;

            Vector3 spawnPos = bossPos + bossDirection * spawnDistance;
            GameObject earthShatter = Instantiate(earthShatterPrefab, spawnPos, Quaternion.identity);
            earthShatter.transform.rotation = bossRotation;
            Destroy(earthShatter, 4);
        }

        public void Scream()
        {
            GameObject scream = Instantiate(screamMagic, model.transform.position, Quaternion.identity);
            scream.transform.eulerAngles = new Vector3(90, 0, 0);
            Destroy(scream, 4);
        }

        public void SwordsFromSkyAttack()
        {
            StartCoroutine(DropSwordsFromSky(15));
        }

        IEnumerator DropSwordsFromSky(int counter)
        {
            float x_offset = Random.Range(-1, 1);
            float z_offset = Random.Range(-1, 1);
            GameObject earth = Instantiate(magicSwordFromSky, new Vector3(player.transform.position.x + x_offset, player.transform.position.y + 6, player.transform.position.z + z_offset), Quaternion.identity);
            yield return new WaitForSeconds(0.25f);
            if (counter > 0)
                StartCoroutine(DropSwordsFromSky(counter - 1));
        }

        public void CastAura()
        {
            if (!IsBossTakingDamage())
            {
                Vector3 spawnPos = model.transform.position;
                //spawnPos.y = 0.02f;
                GameObject aura = Instantiate(auraMagic, spawnPos, Quaternion.identity);
                aura.transform.eulerAngles = new Vector3(-90, 0, 0);
                aura.transform.position += new Vector3(0, 0.2f, 0);
            }
        }

        public void FireSpell()
        {
            if (!IsBossTakingDamage())
            {
                Vector3 relativePos = player.position - spellPosition.position;
                Instantiate(spell, spellPosition.position, Quaternion.LookRotation(relativePos, Vector3.up));
            }
        }

        public void Impact()
        {
            GameObject impactObj = Instantiate(impactPrefab, impactPosition.position, Quaternion.identity);
            Destroy(impactObj, 1.5f);
        }

        public void LightGreatSwordUp()
        {
            greatSword.gameObject.GetComponent<SwordBossScript>().EnableGreatSwordFire();

            Vector3 size = new Vector3(0f, 2.09f, 0.013f);
            Vector3 center = new Vector3(0f, 1.2f, 0f);
            SetGreatSwordSize(size, center);
            greatSword.gameObject.GetComponent<SwordBossScript>().customSize += new Vector3(0, 0, 0.012f);
        }
        private void SetGreatSwordSize(Vector3 size, Vector3 center)
        {
            greatSword.gameObject.GetComponent<BoxCollider>().size = size;
            greatSword.gameObject.GetComponent<BoxCollider>().center = center;
        }

        private void MagicFarSword()
        {
            GameObject obj = Instantiate(magicFarSword, greatSword.transform.position, Quaternion.identity);
            Destroy(obj, 4.5f);
        }

        #endregion

        #region Kick

        public void TurnKickColliderOn()
        {
            leftFootCollider.enabled = true;
            leftFootCollider.GetComponent<DamageDealer>().damageOn = true;
        }

        public void TurnKickColliderOff()
        {
            leftFootCollider.enabled = false;
            leftFootCollider.GetComponent<DamageDealer>().damageOn = false;
        }

        #endregion

        private void SetNotAttackingFalse()
        {
            anim.SetBool("NotAttacking", false);
        }

        private void SetNotAttackingTrue()
        {
            anim.SetBool("NotAttacking", true);
        }

        private void SetCanRotateTrue()
        {
            anim.SetBool("CanRotate", true);
        }

        private void SetCanRotateFalse()
        {
            anim.SetBool("CanRotate", false);
        }
    }
}