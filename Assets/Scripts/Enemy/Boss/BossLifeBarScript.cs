using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SG
{
    public class BossLifeBarScript : MonoBehaviour
    {
        private GameObject lifeBarParent;

        public float maxLife = 40;
        private float life = 0;
        private float filler = 30;
        private float ghost = 0;
        private int barHeight = 17;
        public Animator bossAnim;

        [Header("LifeBar")]
        public Image lifeBar;
        public Image lifeGhost;
        private Animator lifeBarAnim;

        private float lastTime;
        private float waitTime = 1.5f;

        [HideInInspector]
        public bool fillBossLifeBar = false;

        [Header("Win")]
        public GameObject winnerScreen;
        public GameObject bonfire;
        public GameObject winEffect;
        public AudioSource musicSource;
        public GameObject credits;

        public GameManager gameManager;

        private void Start()
        {
            lifeBarParent = this.transform.parent.gameObject;
            this.GetComponent<CanvasGroup>().alpha = 0;
            life = maxLife;
            lifeBarAnim = lifeBar.GetComponent<Animator>();
        }

        private void Update()
        {
            
        }

        private void FixedUpdate()
        {
            if (life <= ((maxLife * 50) / 100) && !bossAnim.GetBool("Phase2"))
            {
                bossAnim.SetTrigger("BeginPhase2");
                bossAnim.SetBool("Phase2", true);
            }

            if (life > ghost && !lifeBarAnim.enabled)
            {
                ghost = life;
                lifeGhost.rectTransform.sizeDelta = new Vector2(ghost * filler, barHeight);
            }

            if ((Time.time > lastTime + waitTime) && ghost > life)
            {
                ghost -= 0.1f;
                lifeGhost.rectTransform.sizeDelta = new Vector2(Mathf.Lerp(ghost, life, 8 * Time.deltaTime) * filler, barHeight);
            }

            if (this.GetComponent<CanvasGroup>().alpha == 1 && lifeBarAnim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1)
                lifeBarAnim.enabled = false;
        }

        public void UpdateLife(float amount)
        {
            Debug.Log(amount);
            if (IsDead()) return;

            if (amount < 0)
            {
                lastTime = Time.time;
            }

            life += amount;

            if (life > maxLife) life = maxLife;
            if (life < 0) life = 0;

            if (life == 0 && !IsDead())
            {
                Die();
            }

            lifeBar.rectTransform.sizeDelta = new Vector2(life * filler, barHeight);
        }

        public void FillBossLifeBar()
        {
            this.GetComponent<CanvasGroup>().alpha = 1;
            lifeBar.gameObject.SetActive(true);
        }

        private bool IsDead()
        {
            return bossAnim.GetBool("Dead");
        }

        private void Die()
        {
            bossAnim.SetBool("Dead", true);
            bossAnim.SetFloat("Vertical", 0);
            bossAnim.SetFloat("Horizontal", 0);
            lifeBar.gameObject.SetActive(false);
            StartCoroutine(AfterWin());
            GameManager.isBossDead = true; //Se debe configurar
        }

        public float GetBossLifeAmount()
        {
            return life;
        }

        IEnumerator AfterWin()
        {
            StartCoroutine(FadeMusic());
            yield return new WaitForSeconds(1.5f);
            Vector3 offset = new Vector3(0, 0, 1);
            Instantiate(winEffect, bossAnim.gameObject.transform.position + offset, Quaternion.identity);
            winnerScreen.SetActive(true);
            bossAnim.gameObject.GetComponent<CapsuleCollider>().isTrigger = true;
            CapsuleCollider[] legs = bossAnim.gameObject.GetComponentsInChildren<CapsuleCollider>();
            foreach (CapsuleCollider leg in legs) leg.isTrigger = true;
            //girlScript.DisableEstusFlask(); //Se debe configurar
            musicSource.volume = 0;
            this.gameObject.SetActive(false);
        }

        IEnumerator FadeMusic()
        {
            yield return new WaitForSeconds(0.25f);
            musicSource.volume -= 0.225f;
            if (musicSource.volume > 0)
                StartCoroutine(FadeMusic());
        }
    }
}
