using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG
{
    public class HitManager : StateMachineBehaviour
    {
        private GameObject boss;
        public int hit;

        // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            boss = GameObject.FindGameObjectWithTag("Boss").transform.parent.gameObject;
            if (boss != null)
            {
                boss.GetComponent<BossScript>().SwordHit(hit);
            }
            else
            {
                //
            }
        }
    }
}