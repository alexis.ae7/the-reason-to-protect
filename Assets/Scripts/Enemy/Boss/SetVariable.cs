using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG
{
    public class SetVariable : StateMachineBehaviour
    {
        public bool onEnter;
        public bool onExit;
        public string varName;
        public bool state;

        // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (onEnter)
                animator.SetBool(varName, state);
        }

        override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (onExit)
                animator.SetBool(varName, state);
        }
    }
}
