using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG
{
    public class DamageDealer : MonoBehaviour
    {
        public bool damageOn;
        public float damageAmount;
                                   
        public AudioClip[] impactSound; 

        private float lastSoundTime = 0;

        public float GetDamage() 
        {
            return damageAmount;
        }

        private void OnTriggerEnter(Collider collision)
        {
            if (!damageOn) return; 

            //if (/*other.gameObject.layer != 9 && */other.gameObject.layer != 11 && other.gameObject.layer != 13) return;

            if (collision.tag == "Player")
            {
                if (collision.transform.GetComponentInChildren<Animator>().GetBool("isInvulnerable")) return;
                collision.transform.GetComponent<PlayerStats>().TakeDamage(25);
            }

            if (SoundInterval() && impactSound.Length > 0)
            {
                //SoundManager.CreateAndPlay(impactSound[Random.Range(0, impactSound.Length)], GameObject.FindGameObjectWithTag("SoundManager").gameObject, other.transform, 2);
                lastSoundTime = Time.time;
            }
        }

        public void GreatSwordFiller(GameObject other)
        {
            if (!damageOn) return; 

            if (other.gameObject.layer != 11 && other.gameObject.layer != 13) return; // nao atinge o que nao for da layer Ground, Player ou Scenary

            if (other.gameObject.name == "Girl") 
            {
                if (other.GetComponent<Animator>().GetBool("Intangible")) return; 
                //other.transform.GetComponentInParent<GirlScript>().RegisterDamage(damageAmount); SE debe revisar
            }

            if (SoundInterval() && impactSound.Length > 0) 
            {
                SoundManager.CreateAndPlay(impactSound[Random.Range(0, impactSound.Length)], GameObject.FindGameObjectWithTag("SoundManager").gameObject, other.transform, 2); 
                lastSoundTime = Time.time;
            }
        }

        private bool SoundInterval()
        {
            return Time.time > lastSoundTime + 0.5f;
        }
    }
}