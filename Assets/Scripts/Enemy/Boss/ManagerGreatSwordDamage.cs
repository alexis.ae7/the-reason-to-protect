using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG
{
    public class ManagerGreatSwordDamage : StateMachineBehaviour
    {
        public int damageAmount;

        // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            animator.gameObject.GetComponent<BossAttacks>().greatSword.damageAmount = damageAmount; // atualiza o dano da GreatSword

            if (animator.GetBool("Phase2")) // incrementa a forca em 0.5 caso seja a phase 2 do boss
            {
                animator.gameObject.GetComponent<BossAttacks>().greatSword.damageAmount = damageAmount + 5f; // atualiza o dano da GreatSword
            }
        }
    }
}