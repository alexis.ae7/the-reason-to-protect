﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG
{
    public class AuraMagic : MonoBehaviour
    {
        private Transform player;
        private bool isEnabled = true;

        BossScript boss;

        private void Start()
        {
            //player = GameObject.FindGameObjectWithTag("Player").transform;
            boss = FindObjectOfType<BossScript>();
            player = boss.player.transform;
            StartCoroutine(DisableAura());
        }

        // Update is called once per frame
        void Update()
        {
            if (Vector3.Distance(this.transform.position, player.position) < 5.5f && isEnabled)
            {
                player.GetComponentInParent<PlayerManager>().insideAuraMagic = true;
            }
            else
            {
                player.GetComponentInParent<PlayerManager>().insideAuraMagic = false;
            }
        }

        IEnumerator DisableAura() // espera 3seg para desativar a aura
        {
            yield return new WaitForSeconds(3);
            isEnabled = false; // desativa o poder da aura
            player.GetComponentInParent<PlayerManager>().insideAuraMagic = false;
            Destroy(this.gameObject);
        }
    }
}