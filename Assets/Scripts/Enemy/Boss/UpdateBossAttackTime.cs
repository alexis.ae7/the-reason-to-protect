using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG
{
    public class UpdateBossAttackTime : StateMachineBehaviour
    {
        // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            animator.gameObject.GetComponent<BossAttacks>().CallNextMove(); // avisa o script boss attack de que o ataque acabou
        }
    }
}