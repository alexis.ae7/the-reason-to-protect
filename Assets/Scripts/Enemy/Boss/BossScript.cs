using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SG
{
    public class BossScript : MonoBehaviour
    {
        public Transform model;
        public Transform greatSword;

        private Animator anim;
        public Transform player;
        //public GirlScript player; Pendiente de revision

        public AudioClip[] takeDamageSound;
        public BossLifeBarScript bossLifeScript;

        public GameObject bloodPrefab;
        public Transform bloodPos;

        public LayerMask layerPlayer;

        private float rotationSpeed = 6;
        public BossAttacks bossAttacks;

        private float lastDamageTakenTime = 0;

        private int hit = 0;
        private int currentHit = 0;


        // Start is called before the first frame update
        void Start()
        {
            anim = model.GetComponent<Animator>();
            bossAttacks = GetComponentInChildren<BossAttacks>();
        }

        // Update is called once per frame
        void Update()
        {
            if (player == null)
            {
                Collider[] colliders = Physics.OverlapSphere(transform.position, 80f, layerPlayer);

                for (int i = 0; i < colliders.Length; i++)
                {
                    player = colliders[i].transform.transform;
                    Debug.Log(player.Find("Bot Position").transform);
                    bossAttacks.player = colliders[i].transform.transform;
                    bossAttacks.playerAnim = colliders[i].transform.GetComponentInChildren<Animator>();
                }
            }

            if (player != null)
            {
                if (anim.GetBool("Dead")) return;

                if (anim.GetCurrentAnimatorStateInfo(0).IsName("idle") && anim.GetCurrentAnimatorStateInfo(1).IsName("None") || !anim.GetBool("CanRotate"))
                {
                    Vector3 rotationOffset = player.transform.position - model.position;
                    rotationOffset.y = 0;
                    float lookDirection = Vector3.SignedAngle(model.forward, rotationOffset, Vector3.up);
                    anim.SetFloat("LookDirection", lookDirection);
                }
                else if (!anim.GetBool("Attacking") && anim.GetBool("CanRotate"))
                {
                    //model.transform.LookAt(player.transform.position); // olha para o player caso nao esteja atacando

                    var targetRotation = Quaternion.LookRotation(player.transform.position - model.transform.position);

                    // Smoothly rotate towards the target point.
                    model.transform.rotation = Quaternion.Slerp(model.transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
                }

                model.transform.eulerAngles = new Vector3(0, model.transform.eulerAngles.y, 0);
                //model.transform.position = new Vector3(model.transform.position.x, 0, model.transform.position.z);
            }

        }

        private void GreatSwordCollider(bool b)
        {
            greatSword.GetComponent<BoxCollider>().isTrigger = !b;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "Sword" /*&& other.gameObject.GetComponent<Animator>().GetBool("Attacking") */&& DamageInterval() && !anim.GetBool("Dead"))
            {
                lastDamageTakenTime = Time.time;
                CreateAndPlay(takeDamageSound[UnityEngine.Random.Range(0, takeDamageSound.Length)], 2);
                //StopAllCoroutines();
                HitManager();
                //DamageBoss();
                if (!anim.GetBool("TakingDamage") && !anim.GetBool("Attacking") && anim.GetBool("NotAttacking"))
                    anim.SetTrigger("TakeDamage");

                GameObject blood = Instantiate(bloodPrefab, bloodPos.position, Quaternion.identity);
                blood.transform.LookAt(player.position);
                Destroy(blood, 0.2f);
            }
        }

        public void RegisterPlayerSwordFillDamage()
        {
            if (!anim.GetBool("Attacking") && DamageInterval() && !anim.GetBool("Dead"))
                lastDamageTakenTime = Time.time;
            CreateAndPlay(takeDamageSound[UnityEngine.Random.Range(0, takeDamageSound.Length)], 2);
            //StopAllCoroutines();
            HitManager();
            if (!anim.GetBool("TakingDamage") && !anim.GetBool("Attacking") && anim.GetBool("NotAttacking"))
                anim.SetTrigger("TakeDamage");

            GameObject blood = Instantiate(bloodPrefab, bloodPos.position, Quaternion.identity);
            blood.transform.LookAt(player.position);
            Destroy(blood, 0.2f);
        }
    
        public void SwordHit(int hit)
        {
            this.hit = hit;
            if (hit == 0)
                ClearCurrentHit();
        }

        public void ClearCurrentHit()
        {
            this.currentHit = 0;
        }

        public void DamageBoss()
        {
            bossLifeScript.UpdateLife(-5);
        }

        public void HitManager()
        {
            if (currentHit == 0 && hit == 4)
            {
                bossLifeScript.UpdateLife(-1.5f);
                return;
            }

            currentHit++;

            if (hit == 1) currentHit = 1;

            if (currentHit == 1 && hit == 1)
            {
                bossLifeScript.UpdateLife(-1);
            }
            else if (currentHit == 1 && hit == 4)
            {
                bossLifeScript.UpdateLife(-1.75f);
            }
            else if (currentHit == 0 && hit == 4)
            {
                bossLifeScript.UpdateLife(-1.5f);
            }

            if (currentHit == 2 && hit == 2)
            {
                bossLifeScript.UpdateLife(-1.5f);
            }
            else if (currentHit == 2 && hit == 4)
            {
                bossLifeScript.UpdateLife(-1.75f);
            }

            if (currentHit == 3 && hit == 3)
            {
                bossLifeScript.UpdateLife(-1.75f);
            }
            else if (currentHit == 3 && hit == 4)
            {
                bossLifeScript.UpdateLife(-2f);
            }

            if (currentHit == 4)
            {
                bossLifeScript.UpdateLife(-2.5f);
            }
        }

        private bool DamageInterval()
        {
            return (Time.time > lastDamageTakenTime + 0.7f);
        }

        private void CreateAndPlay(AudioClip clip, float destructionTime, float volume = 1f)
        {
            AudioSource audioSource = gameObject.AddComponent<AudioSource>();
            audioSource.clip = clip;
            audioSource.volume = volume;
            audioSource.Play();
            Destroy(audioSource, destructionTime);
        }
    }
}
