using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG
{
    public class EnemyWeaponSlotManager : MonoBehaviour
    {
        public WeaponItem rightHandWeapon;
        public WeaponItem leftHandWeapon;
        public WeaponItem arrow;
        public GameObject arrowPrefab;

        public Animator animator;
        public EnemyManager enemyManager;

        WeaponHolderSlot rightHandSlot;
        WeaponHolderSlot leftHandSlot;
        WeaponHolderSlot backSlot;
        WeaponHolderSlot leftWeaponSlot;

        DamageCollider leftHandDamageCollider;
        DamageCollider rightHandDamageCollider;

        private void Awake()
        {
            animator = GetComponent<Animator>();
            enemyManager = GetComponentInParent<EnemyManager>();

            WeaponHolderSlot[] weaponHolderSlots = GetComponentsInChildren<WeaponHolderSlot>();
            foreach (WeaponHolderSlot weaponSlot in weaponHolderSlots)
            {
                if (weaponSlot.isLeftHandSlot)
                {
                    leftHandSlot = weaponSlot;
                }
                else if (weaponSlot.isRightHandSlot)
                {
                    rightHandSlot = weaponSlot;
                }
                else if (weaponSlot.isBackSlot)
                {
                    backSlot = weaponSlot;
                }
                else if (weaponSlot.isLeftWeaponSlot)
                {
                    leftWeaponSlot = weaponSlot;
                }
            }
        }

        private void Start()
        {
            LoadWeaponsOnBothHands();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.P))
            {
                FromBowToSowrd(true);                
            }
            if (Input.GetKeyDown(KeyCode.O))
            {
                FromBowToSowrd(false);
            }
        }

        public void LoadWeaponOnSlot(WeaponItem weapon, bool isLeft)
        {
            if (enemyManager.isArcher)
            {
                if (isLeft)
                {
                    leftHandSlot.currentWeapon = weapon;
                    leftHandSlot.LoadWeaponModel(weapon);
                    backSlot.UnloadWeaponAndDestroy();

                    if (weapon.name != "Bow")
                    {
                        LoadWeaponsDamageCollider(true);
                    }

                    if (weapon != null)
                    {
                        animator.CrossFade(weapon.left_hand_idle, 0.2f);
                    }
                    else
                    {
                        animator.CrossFade("Left Arm Empty", 0.2f);
                    }
                }
                else
                {
                    if (weapon != null)
                    {
                        animator.CrossFade(weapon.right_hand_idle, 0.2f);
                    }
                    else
                    {
                        animator.CrossFade("Right Arm Empty", 0.2f);
                    }

                    if (enemyManager.bowInHand)
                    {
                        leftWeaponSlot.currentWeapon = weapon;
                        leftWeaponSlot.LoadWeaponModel(weapon);
                        backSlot.UnloadWeaponAndDestroy();
                        rightHandSlot.UnloadWeaponAndDestroy();

                    }
                    else
                    {
                        backSlot.LoadWeaponModel(leftHandSlot.currentWeapon);
                        rightHandSlot.currentWeapon = weapon;
                        rightHandSlot.LoadWeaponModel(weapon);

                        leftHandSlot.UnloadWeaponAndDestroy();
                        leftWeaponSlot.UnloadWeapon();

                        LoadWeaponsDamageCollider(false);
                    }
                }
            }
            else
            {
                if (isLeft)
                {
                    leftHandSlot.currentWeapon = weapon;
                    leftHandSlot.LoadWeaponModel(weapon);
                    if (weapon.name != "Bow")
                    {
                        LoadWeaponsDamageCollider(true);
                    }

                    if (weapon != null)
                    {
                        animator.CrossFade(weapon.left_hand_idle, 0.2f);
                    }
                    else
                    {
                        animator.CrossFade("Left Arm Empty", 0.2f);
                    }
                }
                else
                {
                    if (weapon != null)
                    {
                        animator.CrossFade(weapon.right_hand_idle, 0.2f);
                    }
                    else
                    {
                        animator.CrossFade("Right Arm Empty", 0.2f);
                    }

                    if (enemyManager.bowInHand)
                    {
                        leftWeaponSlot.currentWeapon = weapon;
                        leftWeaponSlot.LoadWeaponModel(weapon);
                    }
                    else
                    {
                        rightHandSlot.currentWeapon = weapon;
                        rightHandSlot.LoadWeaponModel(weapon);
                        LoadWeaponsDamageCollider(false);
                    }
                }
            }
        }

        public void LoadWeaponsOnBothHands()
        {
            if (rightHandWeapon != null)
            {
                LoadWeaponOnSlot(rightHandWeapon, false);
            }
            if (leftHandWeapon != null)
            {
                LoadWeaponOnSlot(leftHandWeapon, true);
            }
        }

        public void LoadWeaponsDamageCollider(bool isLeft)
        {
            if (isLeft)
            {
                leftHandDamageCollider = leftHandSlot.currentWeaponModel.GetComponentInChildren<DamageCollider>();
                leftHandDamageCollider.characterManager = GetComponentInParent<CharacterManager>();
            }
            else
            {
                rightHandDamageCollider = rightHandSlot.currentWeaponModel.GetComponentInChildren<DamageCollider>();
                rightHandDamageCollider.characterManager = GetComponentInParent<CharacterManager>();
            }
        }

        public void OpenDamageCollider()
        {
            rightHandDamageCollider.EnableDamageCollider();
        }

        public void CloseDamageCollider()
        {
            if (rightHandDamageCollider != null)
            {
                rightHandDamageCollider.DisaleDamageCollider();
            }
        }

        public void DrainStaminaLightAttack()
        {

        }

        public void DrainStaminaHeavyAttack()
        {

        }

        public void EnableCombo()
        {
            //anim.SetBool("canDoCombo", true);
        }

        public void DisableCombo()
        {
            //anim.SetBool("canDoCombo", false);
        }

        public void BlaBlaBla()
        {
            StartCoroutine("ShootArrow");
        }

        public void FromBowToSowrd(bool isTrue)
        {

            enemyManager.bowInHand = isTrue;

            if (enemyManager.bowInHand)
            {
                LoadWeaponOnSlot(leftHandWeapon, true);
                LoadWeaponOnSlot(rightHandWeapon, false);
            }
            else
            {
                LoadWeaponOnSlot(rightHandWeapon, false);
            }
        }

        IEnumerator ShootArrow()
        {
            WeaponManager weaponManager = leftHandSlot.currentWeaponModel.GetComponent<WeaponManager>();
            yield return new WaitForSeconds(0.2f);
            rightHandSlot.currentWeapon = arrow;
            rightHandSlot.LoadWeaponModel(arrow);
            yield return new WaitForSeconds(0.5f);
            weaponManager.ShootArrow(arrowPrefab);
            rightHandSlot.UnloadWeapon();
            
        }
    }
}