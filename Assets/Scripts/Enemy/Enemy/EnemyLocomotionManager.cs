using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG
{
    public class EnemyLocomotionManager : MonoBehaviour
    {
        EnemyManager enemyManager;
        EnemyAnimatorManager enemyAnimatorManager;
        public Vector3 moveDirection;

        public CapsuleCollider characterColiider;
        public CapsuleCollider characterCollisionBlockerColiider;
        public new Rigidbody rigidbody;
        public float movementSpeed = 5;

        Transform myTransform;
        float groundDetectionRayStartPoint = 0.5f;
        float fallingSpeed = 45;
        float groundDirectionRayDistance = 0.2f;
        float minimumDistanceNeededToBeginFall = 1f;
        LayerMask ignoreForGroundCheck;
        public float inAirTimer;

        public LayerMask detectionLayer;

        private void Awake()
        {
            enemyManager = GetComponent<EnemyManager>();
            enemyAnimatorManager = GetComponentInChildren<EnemyAnimatorManager>();
            rigidbody = GetComponent<Rigidbody>();
        }

        private void Start()
        {
            Physics.IgnoreCollision(characterColiider, characterCollisionBlockerColiider, true);
        }

        private void FixedUpdate()
        {
            rigidbody.AddForce(-Vector3.up * fallingSpeed);
            rigidbody.AddForce(moveDirection * fallingSpeed / 10f);
        }
    }
}