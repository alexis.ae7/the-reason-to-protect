using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG
{
    public class EnemyStats : CharacterStats
    {
        EnemyAnimatorManager enemyAnimatorManager;
        EnemyManager enemyManager;

        public int expAwardedOnDeath = 50;

        public UIEnemyHealthBar enemyHealthBar;

        private void Awake()
        {
            enemyManager = GetComponent<EnemyManager>();
            enemyAnimatorManager = GetComponentInChildren<EnemyAnimatorManager>();
        }

        // Start is called before the first frame update
        void Start()
        {
            maxHealth = SetMaxHealthFromHealthLevel();
            currentHealth = maxHealth;
            enemyHealthBar.SetMaxHealth(maxHealth);
        }

        private int SetMaxHealthFromHealthLevel()
        {
            maxHealth = healthLevel * 10;
            return maxHealth;
        }

        public void TakeDamageNoAnimator(int damage)
        {
            currentHealth = currentHealth - damage;
            enemyHealthBar.SetHealth(currentHealth);

            if (currentHealth <= 0)
            {
                HandleDeath();
            }
        }

        public void TakeDamage(int damage, string damageAnimator = "Damage_01")
        {
            if (isDead)
                return;

            if(enemyManager.currentTarget == null)
            {
                PlayerManager playerManager = FindObjectOfType<PlayerManager>();
                CharacterStats characterStats = playerManager.GetComponent<CharacterStats>();
                enemyManager.currentTarget = characterStats;
            }

            currentHealth = currentHealth - damage;
            enemyHealthBar.SetHealth(currentHealth);

            enemyAnimatorManager.PlayTargetAnimation(damageAnimator, true);

            if (currentHealth <= 0)
            {
                HandleDeath();
            }
        }

        private void HandleDeath()
        {
            currentHealth = 0;
            isDead = true;
            enemyAnimatorManager.PlayTargetAnimation("Death_01", true);
            InputHandler player = FindObjectOfType<InputHandler>();
            CameraHandler cameraPlayer = FindObjectOfType<CameraHandler>();
            player.lockOnFlag = false;
            cameraPlayer.ClearLockOnTargets();
            Destroy(this.gameObject, 4);
        }
    }
}
