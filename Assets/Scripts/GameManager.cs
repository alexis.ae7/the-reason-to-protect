using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SG
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager instancia = null;

        public static GameObject player;
        public static GameObject gui;

        [Header("Public Variables")]
        public static Vector3 saveinitialPoint;
        public Animator transitionLoadScene;
        public Animator transitionFadeResetScene;

        [Header("Static Variables")]
        public static bool playerIsDead;
        public static bool changeLevel = false;
        public static bool canchangeLevel = false;
        public static int healEstatusAmoung = 25;
        public static int maxEstatus = 5;
        public static int enemiesKilled;
        public static bool isBossDead = false;
        public static bool reloadSceneBoss = false;

        int maxHealth;
        float maxStamina;

        int estatusSave;
        int saveHealEstatusAmoung;

        int expSave;

        float widthHealth;
        float widthStamina;

        private void Awake()
        {
            if (instancia == null)
            {
                instancia = this;
            }
            else if (instancia != this)
            {
                Destroy(gameObject);
            }
            DontDestroyOnLoad(gameObject);
        }


        // Start is called before the first frame update
        void Start()
        {
            player = GameObject.FindGameObjectWithTag("Player");
            gui = GameObject.FindGameObjectWithTag("GUI");

            gui.GetComponentInChildren<EstusCount>().estusCount.text = maxEstatus.ToString();

            SaveDataPlayer();

            HideCursor(true);
        }

        // Update is called once per frame
        void Update()
        {
            if (player.GetComponent<InputHandler>().inventoryFlag == false)
            {
                if (SceneManager.GetActiveScene().buildIndex == 1 && canchangeLevel == false)
                {
                    gui.GetComponentInChildren<ObjectivesUI>().objectivesText.text = "Eliminate 12 enemies";
                }
                else if (SceneManager.GetActiveScene().buildIndex == 2 && canchangeLevel == false)
                {
                    gui.GetComponentInChildren<ObjectivesUI>().objectivesText.text = "Eliminate 24 enemies";
                }
                else if (SceneManager.GetActiveScene().buildIndex == 3 && canchangeLevel == false)
                {
                    gui.GetComponentInChildren<ObjectivesUI>().objectivesText.text = "Eliminate the traitor";
                }
            }

            if (Input.GetKeyDown(KeyCode.R) && playerIsDead)
            {
                ReloadScene();
            }

            if (Input.GetKeyDown(KeyCode.V))
            {
                IncreaseLevelStats();
            }
        }

        public static void HideCursor(bool b)
        {
            if (b) // hide
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
            else // visible
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
        }

        public static void IncreaseLevelStats()
        {
            player.GetComponent<PlayerStats>().IncreaseStamina(1);
            player.GetComponent<PlayerStats>().IncreaseHealth(1);
            gui.GetComponentInChildren<HealthBar>().IncreaseWidthBar(20);
            gui.GetComponentInChildren<StaminaBar>().IncreaseWidthBar(20);
            maxEstatus = maxEstatus + 1;
            healEstatusAmoung = healEstatusAmoung + 5;
            SetEstatus(maxEstatus);
        }

        void ReloadScene()
        {
            if (SceneManager.GetActiveScene().buildIndex == 3)
            {
                reloadSceneBoss = true;
                playerIsDead = false;
                gui.GetComponentInChildren<EnemiesKilledBar>().SetEnemiesCountText(0);
                gui.GetComponentInChildren<expCountBar>().SetExpCountText(expSave);
                player.GetComponent<PlayerStats>().Resetstats(maxHealth, maxStamina);
                player.GetComponent<PlayerStats>().expCount = expSave;
                gui.GetComponentInChildren<StaminaBar>().ResetWidthBar(widthStamina);
                gui.GetComponentInChildren<HealthBar>().ResetWidthBar(widthHealth);
                maxEstatus = estatusSave;
                healEstatusAmoung = saveHealEstatusAmoung;
            }
            else
            {
                StartCoroutine(TransitionOnFadeIn());
            }
        }


        public void LoadNextLevel()
        {
            changeLevel = true;
            SaveDataPlayer();
        }

        public static void MoveInitialPoint( Vector3 initialPoint)
        {
            player.transform.position = initialPoint;
            saveinitialPoint = initialPoint;
        }

        IEnumerator TransitionOnFadeIn()
        {
            transitionFadeResetScene.SetBool("Start", playerIsDead);
            yield return new WaitForSeconds(5);
            ResetValues();
            StartCoroutine(TransitionOnFadeOut());
        }

        IEnumerator TransitionOnFadeOut()
        {
            //transitionFadeResetScene.gameObject.SetActive(false);
            yield return new WaitForSeconds(2);
            playerIsDead = false;
            transitionFadeResetScene.SetBool("Start", playerIsDead); 
        }

        private void SaveDataPlayer()
        {
            widthHealth = gui.GetComponentInChildren<HealthBar>().rectTransform.sizeDelta.x;
            widthStamina = gui.GetComponentInChildren<StaminaBar>().rectTransform.sizeDelta.x;
            maxHealth = player.GetComponent<CharacterStats>().currentHealth;
            maxStamina = player.GetComponent<CharacterStats>().currentStamina;

            estatusSave = maxEstatus;
            saveHealEstatusAmoung = healEstatusAmoung;

            expSave = player.GetComponent<PlayerStats>().expCount;
        }

        public static void SetEstatus(int currentEstus)
        {
            gui.GetComponentInChildren<EstusCount>().estusCount.text = currentEstus.ToString();
        }

        public void ResetValues()
        {
            player.GetComponent<PlayerStats>().resetPlayer();
            playerIsDead = false;
            player.transform.position = saveinitialPoint;
            gui.GetComponentInChildren<EnemiesKilledBar>().SetEnemiesCountText(0);
            gui.GetComponentInChildren<expCountBar>().SetExpCountText(expSave);
            player.GetComponent<PlayerStats>().Resetstats(maxHealth, maxStamina);
            player.GetComponent<PlayerStats>().expCount = expSave;
            gui.GetComponentInChildren<StaminaBar>().ResetWidthBar(widthStamina);
            gui.GetComponentInChildren<HealthBar>().ResetWidthBar(widthHealth);
            maxEstatus = estatusSave;
            healEstatusAmoung = saveHealEstatusAmoung;
            SetEstatus(estatusSave);
            ReloadEnemies();
        }

        public static void EnemiesKilled()
        {
            enemiesKilled = enemiesKilled + 1;
            gui.GetComponentInChildren<EnemiesKilledBar>().SetEnemiesCountText(enemiesKilled);

            if(SceneManager.GetActiveScene().buildIndex == 1)
            {
                if (enemiesKilled == 12)
                {
                    canchangeLevel = true;
                    gui.GetComponentInChildren<ObjectivesUI>().objectivesText.text = "Go back to the start and knock on the door";
                    enemiesKilled = 0;
                }
            }
            else if (SceneManager.GetActiveScene().buildIndex == 2)
            {
                if (enemiesKilled == 24)
                {
                    canchangeLevel = true;
                    gui.GetComponentInChildren<ObjectivesUI>().objectivesText.text = "Go back to the start and knock on the door";
                    enemiesKilled = 0;
                }
            }
        }

        public void ReloadEnemies()
        {
            var enemiesSpawn = GameObject.FindObjectsOfType<SpawnEnemies>();
            GameObject[] enemiesinscene = GameObject.FindGameObjectsWithTag("Enemy");

            for (int i = 0; i < enemiesinscene.Length; i++)
            {
                Destroy(enemiesinscene[i]);
            }

            for (int i = 0; i < enemiesSpawn.Length; i++)
            {
                enemiesSpawn[i].SpawnEnemiesMethos();
            }
        }
    }
}
