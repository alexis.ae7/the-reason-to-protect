using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace SG
{
    public class Credits : MonoBehaviour
    {
        public AudioSource creditsSource;

        private void CreditsEnd() // Chamado pelo final da animacao dos creditos, vai pro menu
        {
            GameManager.gui.SetActive(enabled);
            GameManager.player.GetComponent<InputHandler>().enabled = true;
            SceneManager.LoadScene(0);
        }

        private void PlayCreditsMusic()
        {
            GameManager.gui.SetActive(false);
            GameManager.player.GetComponent<InputHandler>().enabled = false;
            creditsSource.enabled = true;
        }
    }
}