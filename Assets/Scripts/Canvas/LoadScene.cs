using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SG {
    public class LoadScene : MonoBehaviour
    {
        Animator anim;
        public int timeToWait;
        public Transform initialPoint;
        public GameObject credits;
        bool inicioDeEscena;

        private void Awake()
        {
            anim = GetComponent<Animator>();
            inicioDeEscena = true;
        }

        private void Start()
        {
            
        }

        private void Update()
        {
            if (inicioDeEscena)
            {
                StartCoroutine(InicioDeEscena(timeToWait));
                inicioDeEscena = false;
            }

            if (GameManager.isBossDead)
            {
                StartCoroutine("CreditosFinales");
                GameManager.isBossDead = false;
            }

        }

        private void FixedUpdate()
        {
            if (GameManager.changeLevel)
            {
                LoadNextLevel(1);
                GameManager.changeLevel = false;
            }

            if (GameManager.isBossDead)
            {
                GameManager.isBossDead = false;
                StartCoroutine("EscenaCreditos");
            }

            if (GameManager.reloadSceneBoss)
            {
                GameManager.reloadSceneBoss = false;
                LoadNextLevel(0);
            }
    }

        public void LoadNextLevel(int level)
        {
            StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex + level));
        }

        IEnumerator LoadLevel(int levelIndex)
        {
            anim.SetTrigger("start");
            yield return new WaitForSeconds(4);
            GameManager.player.GetComponent<PlayerStats>().resetPlayer();
            GameManager.player.GetComponent<InputHandler>().lockOnFlag = false;
            SceneManager.LoadScene(levelIndex);
        }

        IEnumerator InicioDeEscena(int timeToWait)
        {
            GameManager.MoveInitialPoint(initialPoint.transform.position);
            GameManager.gui.GetComponentInChildren<EnemiesKilledBar>().SetEnemiesCountText(0);
            yield return new WaitForSeconds(timeToWait);
            anim.SetTrigger("LoadScene");
        }

        IEnumerator CreditosFinales()
        {
            yield return new WaitForSeconds(4);
            credits.SetActive(true);
        }
    }
}