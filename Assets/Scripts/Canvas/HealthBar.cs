using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SG
{
    public class HealthBar : MonoBehaviour
    {
        public Slider slider;
        public RectTransform rectTransform;

        private void Start()
        {
            slider = GetComponent<Slider>();
            rectTransform = GetComponent<RectTransform>();
        }

        public void SetMaxHeath(int maxHealth)
        {
            slider.maxValue = maxHealth;
            slider.value = maxHealth;
        }

        public void SetCurrentHealth(int currentHealth)
        {
            slider.value = currentHealth;
        }

        public void IncreaseWidthBar(int level)
        {
            rectTransform.sizeDelta = new Vector2(rectTransform.sizeDelta.x + level, rectTransform.sizeDelta.y);
        }

        public void ResetWidthBar(float level)
        {
            rectTransform.sizeDelta = new Vector2(level, rectTransform.sizeDelta.y);
        }
    }
}