using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemiesKilledBar : MonoBehaviour
{
    public Text enemiesKilled;

    public void SetEnemiesCountText(int enemiesCount)
    {
        enemiesKilled.text = enemiesCount.ToString();
    }
}
