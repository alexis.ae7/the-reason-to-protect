using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

namespace SG
{
    public class StaminaBar : MonoBehaviour
    {
        public Slider slider;
        public RectTransform rectTransform;

        private void Start()
        {
            slider = GetComponent<Slider>();
            rectTransform = GetComponent<RectTransform>();
        }

        public void SetMaxStamina(float maxStamina)
        {
            slider.maxValue = maxStamina;
            slider.value = maxStamina;
        }

        public void SetCurrentStamina(float currentStamina)
        {
            slider.value = currentStamina;
        }

        public void IncreaseWidthBar(int level)
        {
            rectTransform.sizeDelta = new Vector2(rectTransform.sizeDelta.x + level, rectTransform.sizeDelta.y);
        }

        public void ResetWidthBar(float level)
        {
            rectTransform.sizeDelta = new Vector2(level, rectTransform.sizeDelta.y);
        }
    }
}
