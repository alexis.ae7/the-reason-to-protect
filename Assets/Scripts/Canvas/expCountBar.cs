using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SG
{
    public class expCountBar : MonoBehaviour
    {
        public Text expCountText;

        public void SetExpCountText(int expCount)
        {
            expCountText.text = expCount.ToString();
        }
    }
}