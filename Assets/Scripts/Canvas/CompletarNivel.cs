using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG
{
    public class CompletarNivel : MonoBehaviour
    {
        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.tag == "Player")
            {
                if (GameManager.canchangeLevel == true)
                {
                    GameManager.changeLevel = true;
                    GameManager.canchangeLevel = false;
                }
            }
        }
    }
}