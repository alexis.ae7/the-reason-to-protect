using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG {
    public class WeaponManager : MonoBehaviour
    {
        public GameObject initialPointShoot;
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void ShootArrow(GameObject arrow)
        {
            Instantiate(arrow, initialPointShoot.transform.position, initialPointShoot.transform.rotation);
        }
    }
}