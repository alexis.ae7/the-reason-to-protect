using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG { 
public class SpawnEnemies : MonoBehaviour
    {
        Vector3 myTransform;

        public GameObject enemies;

        private void Awake()
        {
            myTransform = this.transform.position;
        }

        private void Start()
        {
            SpawnEnemiesMethos();
        }

        public void SpawnEnemiesMethos()
        {
            GameObject enemy = Instantiate(enemies, myTransform, Quaternion.identity);
        }
    }
}

