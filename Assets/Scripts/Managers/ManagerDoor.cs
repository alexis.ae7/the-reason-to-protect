using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG
{
    public class ManagerDoor : MonoBehaviour
    {
        public Animator anim;
        public BoxCollider colliderDoor;

        private void Awake()
        {
            anim = GetComponentInParent<Animator>();
            colliderDoor = GetComponent<BoxCollider>();
        }

        private void OpenDoor()
        {
            anim.SetBool("puertaAbierta", true);
            colliderDoor.isTrigger = true;
        }

        private void CloseDoor()
        {
            anim.SetBool("puertaAbierta", false);
            colliderDoor.isTrigger = false;
        }

        private void OnCollisionEnter(Collision collision)
        {
            Debug.Log(collision.gameObject.tag);
            if (collision.gameObject.tag == "Player")
            {
                Debug.Log("Toco la puerta");
                OpenDoor();
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.tag == "Player")
            {
                CloseDoor();
            }
        }
    }
}