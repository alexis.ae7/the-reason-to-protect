using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG
{
    public class PlayerEffectsManager : MonoBehaviour
    {
        PlayerStats playerStats;
        WeaponSlotManager weaponSlotManager;
        public GameObject currentParticleFX;
        public GameObject instantiatedFXModel;
        public int amountToBeHealed;


        private void Awake()
        {
            playerStats = GetComponentInParent<PlayerStats>();
            weaponSlotManager = GetComponent<WeaponSlotManager>();
        }

        public void HealPlayerFromEffect()
        {
            StartCoroutine(CourutineHealPlayer());
            /*playerStats.HealPlayer(GameManager.healEstatusAmoung);
            GameObject healParticles = Instantiate(currentParticleFX, playerStats.transform);
            Destroy(instantiatedFXModel.gameObject);
            weaponSlotManager.LoadBothWeaponsOnSlots();*/
        }

        public void EmptyEstatausFlask()
        {
            Destroy(instantiatedFXModel.gameObject, 0.5f);
            weaponSlotManager.LoadBothWeaponsOnSlots();
        }

        IEnumerator CourutineHealPlayer()
        {
            playerStats.HealPlayer(GameManager.healEstatusAmoung);
            GameObject healParticles = Instantiate(currentParticleFX, playerStats.transform);
            yield return new WaitForSeconds(1);
            Destroy(instantiatedFXModel.gameObject);
            weaponSlotManager.LoadBothWeaponsOnSlots();
        }
    }
}