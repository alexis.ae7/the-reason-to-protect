using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.UI;

namespace SG
{
    public class PlayerStats : CharacterStats
    {
        PlayerManager playerManager;
        public HealthBar healthBar;
        public StaminaBar staminaBar;
        public WeaponSlotManager weaponSlotManager;
        PlayerAnimatorManager animatorHandler;

        int levelHearth = 10;
        float levelStamina = 10;

        public float staminaRegenerationAmount = 50;
        public float staminaRegenTimer = 0;

        public GameObject youDiedScreen;
        private ColorGrading colorGradingLayer = null;
        float postprocessingSetting;


        private void Awake()
        {
            weaponSlotManager = GetComponentInChildren<WeaponSlotManager>();
            playerManager = GetComponent<PlayerManager>();
            healthBar = FindObjectOfType<HealthBar>();
            staminaBar = FindObjectOfType<StaminaBar>();
            animatorHandler = GetComponentInChildren<PlayerAnimatorManager>();
        }

        // Start is called before the first frame update
        void Start()
        {
            maxHealth = SetMaxHealthFromHealthLevel(levelHearth);
            currentHealth = maxHealth;
            healthBar.SetMaxHeath(maxHealth);
            healthBar.SetCurrentHealth(currentHealth);

            maxStamina = SetMaxStaminaFromStaminaLevel(levelStamina);
            currentStamina = maxStamina;
            staminaBar.SetMaxStamina(maxStamina);
            staminaBar.SetCurrentStamina(currentStamina);

            PostProcessVolume volume = Camera.main.GetComponent<PostProcessVolume>();
            volume.profile.TryGetSettings(out colorGradingLayer);
            postprocessingSetting = colorGradingLayer.saturation.value;
        }

        private void FixedUpdate()
        {
            if (isDead)
            {
                Die();
            }
        }

        private int SetMaxHealthFromHealthLevel(int level)
        {
            maxHealth = healthLevel * level;
            return maxHealth;
        }

        private float SetMaxStaminaFromStaminaLevel(float level)
        {
            maxStamina = staminaLevel * level;
            return maxStamina;
        }

        public void TakeDamageNoAnimator(int damage)
        {
            currentHealth = currentHealth - damage;
            
            if (currentHealth <= 0)
            {
                currentHealth = 0;
                isDead = true;
            }
        }

        public void TakeDamage(int damage, string damageAnimator = "Damage_01")
        {
            weaponSlotManager.rightHandDamageCollider.DisaleDamageCollider();

            if (playerManager.isInvulnerable)
                return;

            if (isDead)
                return;

            currentHealth = currentHealth - damage;
            healthBar.SetCurrentHealth(currentHealth);

            animatorHandler.PlayTargetAnimation(damageAnimator, true);
            
            if (currentHealth <= 0)
            {
                currentHealth = 0;
                animatorHandler.PlayTargetAnimation("Death_01", true);
                isDead = true;
            }

            if (isDead)
            {
                GameManager.playerIsDead = true;
            }
        }


        public void TakeStaminaDamage(int damage)
        {
            currentStamina = currentStamina - damage;
            if (currentStamina < 0)
            {
                currentStamina = 0;
            }
            staminaBar.SetCurrentStamina(currentStamina);
        }

        public void RegenerateStamina()
        {
            if (playerManager.isInteracting)
            {
                staminaRegenTimer = 0;
            }
            else
            {
                staminaRegenTimer += Time.deltaTime;
                if (currentStamina < maxStamina && staminaRegenTimer > 1f && playerManager.isSprinting == false)
                {
                    currentStamina += staminaRegenerationAmount * Time.deltaTime;
                    staminaBar.SetCurrentStamina(Mathf.RoundToInt(currentStamina));
                }
            }
        }

        public void AddExp(int exp) 
        {
            expCount = expCount + exp;
        }

        public void RemoveExp(int exp)
        {
            expCount = expCount - exp;
        }

        public void Resetstats(int healt, float stamina)
        {
            currentHealth = healt;
            maxHealth = healt;
            healthBar.SetCurrentHealth(currentHealth);
            healthBar.SetMaxHeath(maxHealth);
            levelHearth = 10;

            currentStamina = stamina;
            maxStamina = stamina;
            staminaBar.SetCurrentStamina(currentStamina);
            staminaBar.SetMaxStamina(maxStamina);
            levelStamina = 10;
        }

        public void IncreaseHealth(int level)
        {
            levelHearth = levelHearth + 1;
            maxHealth = SetMaxHealthFromHealthLevel(levelHearth);
            currentHealth = maxHealth;
            healthBar.SetMaxHeath(maxHealth);
            healthBar.SetCurrentHealth(currentHealth);
        }

        public void IncreaseStamina(float level)
        {
            levelStamina = levelStamina + 1;
            maxStamina = SetMaxStaminaFromStaminaLevel(levelStamina);
            currentStamina = maxStamina;
            staminaBar.SetMaxStamina(maxStamina);
            staminaBar.SetCurrentStamina(currentStamina);
        }

        public void HealPlayer(int healAmount)
        {
            currentHealth = currentHealth + healAmount;

            if (currentHealth > maxHealth)
            {
                currentHealth = maxHealth;
            }

            healthBar.SetCurrentHealth(currentHealth);
        }

        public void Die()
        {
            youDiedScreen.SetActive(true);
            colorGradingLayer.saturation.value = Mathf.Lerp(colorGradingLayer.saturation.value, -100, 1 * Time.deltaTime);
            youDiedScreen.GetComponent<CanvasGroup>().alpha = Mathf.Lerp(youDiedScreen.GetComponent<CanvasGroup>().alpha, 1, 0.5f * Time.deltaTime);
        }

        public void resetPlayer()
        {
            isDead = false;
            youDiedScreen.SetActive(false);
            colorGradingLayer.saturation.value = postprocessingSetting;
        }
    }
}