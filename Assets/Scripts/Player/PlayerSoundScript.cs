using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG
{
    public class PlayerSoundScript : AnimatorManager
    {
        public AudioClip heavySwordAttack;
        public AudioClip swordAttack;
        public AudioClip secondHit;
        public AudioClip thirdHit;
        public AudioClip killed;
        public AudioClip dogeRoll;
        public AudioClip standingUp;
        public AudioClip fallOnGround;
        public AudioClip reachWeapon;
        public AudioClip[] footStepSand;
        public AudioClip[] footStepStone;
        public AudioClip[] takeDamage;
        public AudioSource footSource;

        private PlayerManager playerManager;

        private void Awake()
        {
            playerManager = GetComponentInParent<PlayerManager>();
        }

        // Start is called before the first frame update
        void Start()
        {
  
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void PlaySwordAttack()
        {
            CreateAndPlay(swordAttack, 1);
        }

        public void PlayHeavySwordAttack()
        {
            CreateAndPlay(heavySwordAttack, 1);

        }

        public void PlaySecondHit()
        {
            CreateAndPlay(secondHit, 1);
        }

        public void PlayThirdHit()
        {
            CreateAndPlay(thirdHit, 1);
        }

        public void PlayDodgeRoll()
        {
            CreateAndPlay(dogeRoll, 1);
        }

        public void PlayFootStep()
        {
            if (!playerManager.isInteracting)
            {
                footSource.volume = 0.25f;
                footSource.PlayOneShot(footStepSand[Random.Range(0, footStepSand.Length)]);
            }
        }

        public void PlayTakeDamage()
        {
            CreateAndPlay(takeDamage[Random.Range(0, takeDamage.Length)], 1);
        }

        public void PlayKilled()
        {
            CreateAndPlay(killed, 2, 1);
        }

        private void CreateAndPlay(AudioClip clip, float destructionTime, float volume = 1f)
        {
            AudioSource audioSource = gameObject.AddComponent<AudioSource>();
            audioSource.clip = clip;
            audioSource.volume = volume;
            audioSource.minDistance = 10;
            audioSource.maxDistance = 50;
            audioSource.spatialBlend = 1;
            audioSource.Play();
            Destroy(audioSource, destructionTime);
        }
    }
}
